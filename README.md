# GitLab Volunteer Coaching Program

For more information on this program, refer to the [Volunteer Coaches](https://about.gitlab.com/handbook/engineering/volunteer-coaches-for-urgs/) page in our Handbook as our source of truth.

## FAQ

### What is this project?

This project is for planning and organizing GitLab Volunteer Coaching Programs, [using GitLab](https://about.gitlab.com/handbook/product/product-processes/#dogfood-everything).

### What is this intended use case for this GitLab project?

The problem this project solves is that coaches need a way to collaborate, communicate, and work together on shared coaching goals and initiatives.

The use case is dogfooding GitLab - using our own product (specifically the [issue tracker](https://gitlab.com/gitlab-com/coaching/-/issues)) to organize and facilitate GitLab coaching initiatives.

### How can I get involved?

If you are interested in volunteering as a coach and would like to help with this effort, please submit a MR to add yourself to the [Volunteer Coaching Partners](https://about.gitlab.com/handbook/engineering/volunteer-coaches-for-urgs/#partners) section of the handbook, and assign it to the `GitLab Contact` for the Partner(s) you're volunteering to coach.

Additionally, toggle notifications on for any relevant issues in the [issue tracker](https://gitlab.com/gitlab-com/coaching/-/issues).
